#Technology stack
For run gulp tasks - Node.js

Task worker - Gulp

Package manager - yarn / npm

Html preprocessor - Jade

CSS preprocessor - SCSS

Control version - Git (GitLab)

Favicon compiling - RealFaviconGenerator https://realfavicongenerator.net/favicon/gulp

#Start  
To run gulp tasks need install all packages

    cd html
    yarn install / npm install  

#Gulp tasks  

    gulp watch / gulp  
gulp watch task with browserSync  

    gulp build  
build site without favicons, serwis workers, inline styles/script, optimizing images... 

    gulp buildFull  
build site for production

    gulp zip  
create zip from your project  


&nbsp;

&nbsp;

&nbsp;


#Install CMS  

Unzip CMS files to root directory or clone from git.  

Example structure:  
      
    ProjectName  
        -html  
        -design  
        -app  
        -public  
        -resources  
        -...  

#CMS  

Rename "example.env" file to ".env" and change access to DB.  

In root directory run:

    composer install

    php artisan migrate

    php artisan db:seed

    npm install

    bower install

    php artisan key generate

#Adminpanel:  

    Login: admin  
    Password: Admin8888  
  

#Changelog  
###  
    1.2.1  
    - add info to readme.md

###  
    1.2.0  
    - add inline minify styles
    - add serwisWorker (PWA)
    - add compress images
    - add sitemap generator
    - add jade compiler for hidden directory
    - add webfontloader
    - remove autoprefixer
    - remove webp format
    - fix watch hidden path
    
    
###  
    1.1.0  
    - add support custom title and description for pages  
    - add "rel=canonical"  
    - add owl-carousel to components  
    - add example webp/no-webp class to style.scss  
    - add webp/no-webp mixin in jade  
    - add minify style.css  
    - add minify scripts.js  
    - rework head.jade + variables.json  
    - rework readme.md  
    - remove autoprefixer ?  
    - remove global title, description from variables.json  
    - fix gulp tasks  
    - fix package.json  
    - fix favicons  
    - fix heightLines mixin  
       
###  
    1.0.0  
    - base template  