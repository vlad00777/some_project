"use strict";
var gulp = require('gulp');
var sass = require('gulp-sass');
var watch = require('gulp-watch');
var browserSync = require('browser-sync').create();
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var gcmq = require('gulp-group-css-media-queries');
var sourcemaps = require('gulp-sourcemaps');
var gutil = require("gulp-util");
var realFavicon = require('gulp-real-favicon');
var jade = require('gulp-jade');
var fs = require('fs');
var data = require('gulp-data');
var plumber = require('gulp-plumber');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var cleanCSS = require('gulp-clean-css');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var gulpif = require('gulp-if');
var rename = require("gulp-rename");
var merge = require('merge-stream');
var path = require('path');
const debug = require('gulp-debug');
var newer = require('gulp-newer');

const zip = require('gulp-zip');
var sitemap = require('gulp-sitemap');
var sww = require('gulp-sww');
const imagemin = require('gulp-imagemin');
const mozjpeg = require('imagemin-mozjpeg');
const pngquant = require('imagemin-pngquant');
var deletefile = require('gulp-delete-file');
var inlinesource = require('gulp-inline-source');

/**
 * variables
 */
var projectName = "netkata";

var paths = {
    src: {
        hidden: 'src/hidden',
        sass: 'src/sass',
        svg: 'src/svg',
        jadeAll:'src/jade',
        jade: 'src/jade/pages',
        jadeParts: 'src/jade/partials',
        jadeMixins: 'src/jade/mixins',
        favicons: 'src/favicons',
        js: 'src/js/',
        images: 'src/images',
        fonts: 'src/fonts',
        json:'src/jade/system',
        
        main:'src/'
    },
    front: {
        hidden: 'dist/hidden',
        sass: 'dist/css',
        jade: 'dist/',
        jadeParts: 'dist/partials',
        favicons: 'dist/favicons',
        js: 'dist/js',
        images: 'dist/images',
        fonts: 'dist/fonts',

        main: 'dist/'
    },
    cms: {}
};

/**
 * Clean dist folder on start
 */
gulp.task('cleanDist', function () {
    return gulp.src('dist/', {read: false})
        .pipe(clean());
});

/**
 * Transfers
 */
gulp.task('transferHidden', function () {
    return gulp.src(paths.src.hidden+"/**/*.php")
        .pipe(newer(paths.front.hidden))
        .pipe(gulp.dest(paths.front.hidden));
});

gulp.task('transferImage', function () {
    return gulp.src(paths.src.images + "/**/*.{png,jpg,gif,jpeg}")
        .pipe(newer(paths.front.images))
        .pipe(gulp.dest(paths.front.images));
});

gulp.task('optimizeImage', function () {
    return gulp.src(paths.src.images + "/**/*.{png,jpg,gif,jpeg}")
        .pipe(newer(paths.front.images))
        .pipe(imagemin([
            pngquant({
                quality: '90'
            }),
            mozjpeg({
                progressive: true,
                quality: '90'
            })
        ], {
            verbose: true
        }))
        .pipe(gulp.dest(paths.front.images));
});

gulp.task('transferFonts', function () {
    return gulp.src(paths.src.fonts + "/*.{svg,woff,woff2,ttf,otf,eot}")
        .pipe(newer(paths.front.fonts))
        .pipe(gulp.dest(paths.front.fonts));
});

gulp.task('transferOther', function () {
    return gulp.src(["src/.htaccess", "src/polityka_cookies.pdf"])
        .pipe(newer(paths.front.main))
        .pipe(gulp.dest(paths.front.main));
});

gulp.task('transferSW', function () {
    return gulp.src(["src/install-sw.js"])
        .pipe(newer(paths.front.main))
        .pipe(gulp.dest(paths.front.main));
});

gulp.task('transferNotCompilingScripts', function () {
    return gulp.src(paths.src.js + "/components/_*.js")
        .pipe(newer(paths.front.js))
        .pipe(gulp.dest(paths.front.js));
});

/**
 * Generate font from svg
 * @fontName {string} - name of font
 */
var fontName = "myfont";
gulp.task('iconfont', function () {
    return gulp.src([paths.src.svg + "/*.svg"], {base: 'src/'})
        .pipe(newer(paths.front.fonts))
        .pipe(plumber())
        .pipe(iconfontCss({
            fontName: fontName,
            path: 'src/sass/_icons.scss',
            targetPath: "../../" + paths.src.sass + "/components/_icons.scss",
            fontPath: 'fonts/'
        }))
        .pipe(plumber())
        .pipe(iconfont({
            fontName: fontName,
            prependUnicode: true,
            formats: ['ttf', 'eot', 'woff', 'woff2'],
            normalize: true,
            fontHeight: 1001
        }))
        .pipe(gulp.dest(paths.front.fonts))
        .pipe(browserSync.stream());
});

/**
 * Compile Jade2Html
 */
gulp.task('jade', function () {
    return gulp.src(paths.src.jade + "/**/*.jade")
        .pipe(newer(paths.front.jade))
        .pipe(data(function (file) {
            return JSON.parse(
                fs.readFileSync(paths.src.json + "/variables.json")
            );
        }))
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest(paths.front.jade))
        .pipe(browserSync.reload({stream: true}));
});

/**
 * Compile Jade parts
 */
gulp.task('jadeParts', function () {
    return gulp.src(paths.src.jadeParts + "/*.jade")
        .pipe(data(function (file) {
            return JSON.parse(
                fs.readFileSync(paths.src.json + "/variables.json")
            );
        }))
        .pipe(plumber())
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest(paths.front.jadeParts));
});

gulp.task('jadeHidden', function () {
    return gulp.src(paths.src.hidden + "/*.jade")
        .pipe(jade({
            pretty: true
        }))
        .pipe(gulp.dest(paths.front.hidden));
});

/**
 * Generate favicons from favicon_master.png
 */
var FAVICON_DATA_FILE = paths.src.favicons + '/faviconData.json';
gulp.task("favicons", function (done) {
    realFavicon.generateFavicon({
        masterPicture: paths.src.favicons + '/favicon_master.png',
        dest: paths.front.favicons,
        iconsPath: 'favicons',
        design: {
            ios: {
                pictureAspect: 'backgroundAndMargin',
                backgroundColor: '#ffffff',
                margin: '14%',
                assets: {
                    ios6AndPriorIcons: false,
                    ios7AndLaterIcons: false,
                    precomposedIcons: false,
                    declareOnlyDefaultIcon: true
                },
                appName: 'Netkata'
            },
            desktopBrowser: {},
            windows: {
                pictureAspect: 'noChange',
                backgroundColor: '#ffffff',
                onConflict: 'override',
                assets: {
                    windows80Ie10Tile: false,
                    windows10Ie11EdgeTiles: {
                        small: false,
                        medium: true,
                        big: false,
                        rectangle: false
                    }
                },
                appName: 'Netkata'
            },
            androidChrome: {
                pictureAspect: 'noChange',
                themeColor: '#ffffff',
                manifest: {
                    name: 'Netkata',
                    display: 'standalone',
                    orientation: 'notSet',
                    onConflict: 'override',
                    declared: true
                },
                assets: {
                    legacyIcon: false,
                    lowResolutionIcons: false
                }
            },
            safariPinnedTab: {
                pictureAspect: 'blackAndWhite',
                threshold: 71.5625,
                themeColor: '#af9a63'
            }
        },
        settings: {
            compression: 3,
            scalingAlgorithm: 'Mitchell',
            errorOnImageTooSmall: false,
            readmeFile: false,
            htmlCodeFile: false,
            usePathAsIs: false
        },
        markupFile: FAVICON_DATA_FILE
    }, function () {
        done();
    });
});

/**
 * inject favicons on all html pages
 */
gulp.task('inject-favicons', function () {
    return gulp.src(paths.front.jade + "/**/*.html")
    //.pipe(newer(paths.front.jade))
        .pipe(realFavicon.injectFaviconMarkups(JSON.parse(fs.readFileSync(FAVICON_DATA_FILE)).favicon.html_code))
        .pipe(plumber())
        .pipe(gulp.dest(paths.front.jade));
});

gulp.task('default', ['watch']);

/**
 * BrowserSync init
 */
gulp.task('BrowserSync', function () {
    browserSync.init({
        proxy: "http://" + projectName,
        open: false
    });
});

/**
 * Concat, uglify scripts
 */
gulp.task('scripts', function () {
    var folders = ["components", "partials", "scripts"];

    var tasks = folders.map(function (folder) {
        return gulp.src(path.join(paths.src.js, folder, '/**/[^_]*.js'))
            .pipe(newer(paths.front.js + '/' + folder + '.js'))
            .pipe(plumber())
            .pipe(debug({title: 'concat'}))
            .pipe(concat(folder + '.js'))
            .pipe(gulp.dest(paths.front.js))
            .pipe(uglify())
            .pipe(debug({title: 'uglify'}))
            .pipe(rename(folder + '.min.js'))
            .pipe(gulp.dest(paths.front.js))
            .pipe(browserSync.stream());
    });

    return merge(tasks);
});

gulp.task('sass', function () {
    var folders = ["components", "style", "errors"];

    var task2 = folders.map(function (folder) {
            return gulp.src(path.join(paths.src.sass, folder, '/**/*.{scss,sass,css}'))
                .pipe(plumber())
                .pipe(debug({title: 'sass'}))
                .pipe(sass({outputStyle: 'expanded'}))
                .pipe(concat(folder + '.css'))
                .pipe(gulp.dest(paths.front.sass))
                .pipe(debug({title: 'concat'}))
                .pipe(gcmq())
                .pipe(debug({title: 'mq css'}))
                .pipe(gulp.dest(paths.front.sass))
                .pipe(debug({title: 'min'}))
                .pipe(cleanCSS({debug: true}, function (details) {
                    console.log(details.name + ': ' + details.stats.originalSize);
                    console.log(details.name + ': ' + details.stats.minifiedSize);
                }))
            .pipe(rename(folder + '.min.css'))
                .pipe(gulp.dest(paths.front.sass))
                .pipe(browserSync.stream());
    });

    return merge(task2);
});

gulp.task('offline', function () {
    return gulp.src(paths.front.main + "**/*", {cwd: '/'})
        .pipe(sww())
        .pipe(gulp.dest(paths.front.main));
});

gulp.task('sitemap', function () {
    gulp.src([paths.front.main + '/*.html', paths.front.main + '/oferta/*.html'], {
        read: false
    })
        .pipe(sitemap({
            siteUrl: 'https://www.ibif.pl',
            changefreq: 'weekly',
            priority:'0.5',
            lastmod: Date.now(),
            getLoc: function (siteUrl, loc, entry) {
                return loc.replace(/\.\w+$/, '');
            }
        }))
        .pipe(gulp.dest(paths.front.main));
});

gulp.task('inlinesource', function () {
    return gulp.src(paths.front.main + "/*.html")
        .pipe(inlinesource())
        .pipe(gulp.dest(paths.front.main));
});

gulp.task('changeExt', function(){
    return gulp.src(paths.front.hidden + "/**/*.html")
        .pipe(rename(function (path) {
            path.extname = ".php"
        }))
        .pipe(gulp.dest(paths.front.hidden));
});

gulp.task('build', function (callback) {
    runSequence(
        'cleanDist',
        ['transferHidden', 'transferImage', 'transferOther', 'iconfont', 'jade', 'jadeHidden', 'sass', 'scripts', 'favicons'],
        ['transferFonts', 'changeExt'],
        callback
    );
});

gulp.task('buildFull', function (callback) {
    runSequence(
        'cleanDist',
        ['transferHidden', 'optimizeImage', 'transferOther', 'iconfont', 'jade', 'sass', 'scripts', 'favicons', 'transferNotCompilingScripts'],
        ['offline', 'transferSW', 'changeExt', 'transferFonts'],
        ['inlinesource'],
        ['inject-favicons'],
        callback
    );
});

gulp.task('watch', ['build', 'BrowserSync'], function () {
    gulp.watch(paths.src.sass + '/**/*.{scss,sass,css}', ['sass']);
    gulp.watch(paths.src.js + '/**/*.js', ['scripts', 'transferNotCompilingScripts']);
    gulp.watch(paths.src.svg + '/**/*.svg', ['iconfont']);
    gulp.watch(paths.src.font + '/**/*.{woff,woff2,ttf,otf,eot}', ['transferFonts']);
    gulp.watch([paths.src.jadeAll + '/**/*.jade', paths.src.hidden + '/**/*.jade', paths.src.json + '/**/*.json'], ['jade', 'jadeHidden']);
    gulp.watch(paths.src.favicons + '/favicon-master.png', ['favicons', 'inject-favicons']);
    gulp.watch(paths.src.images + '/**/*.{png,jpg,jpeg}', ['transferImage']);
    gulp.watch(paths.src.hidden + '/*.php', ['transferHidden']);
    gulp.watch(paths.src.main + '/*', ['transferOther']);
});

gulp.task('zip', function () {
    gulp.src(paths.front.main + "/**/*")
        .pipe(zip('projekt.zip'))
        .pipe(gulp.dest("./"))
});

gulp.task('offline', function () {
    return gulp.src(paths.front.main + "**/*", {cwd: '/'})
        .pipe(sww())
        .pipe(gulp.dest(paths.front.main));
});

function getFolders(dir) {
    return fs.readdirSync(dir)
        .filter(function (file) {
            return fs.statSync(path.join(dir, file)).isDirectory();
        });
}
//------------------------------------------------------------------------------------------------------//


gulp.task('Migrate2CMS', function (callback) {
    var stream1 = gulp.src("dist/css/style.css")
        .pipe(rename("front.css"))
        .pipe(gulp.dest("../public/css/"));

    var stream2 = gulp.src("dist/css/components.css")
        .pipe(rename("app_front.css"))
        .pipe(gulp.dest("../public/css/"));

    var stream3 = gulp.src("dist/js/scripts.js")
        .pipe(rename("front.js"))
        .pipe(gulp.dest("../public/js/"));

    var stream4 = gulp.src("dist/js/components.js")
        .pipe(rename("app_front.js"))
        .pipe(gulp.dest("../public/js/"));

    var stream5 = gulp.src(paths.images.input)
        .pipe(gulp.dest("../public/images/"));

    var stream6 = gulp.src(paths.fonts.input)
        .pipe(gulp.dest("../public/fonts"));

    var stream7 = gulp.src(paths.favicons.input)
        .pipe(gulp.dest("../public/favicons"));

    return merge(stream1, stream2, stream3, stream4, stream5, stream6, stream7);
});