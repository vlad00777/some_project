$(document).ready(function () {
    
    $(".js-mainCarousel").owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        dots:true,
        items:1,
        smartSpeed:2000,
        autoplay:true,
        autoplayTimeout:10000,
        animateOut: 'fadeOut',
        mouseDrag: false,
        onChange: function (event) {
            $(event.currentTarget).find(".js-animate").removeClass("animation");
        },
        onChanged: function (event) {
            setTimeout(function () {
                $(".owl-item.active").find(".js-animate").addClass("animation");
            }, 1600);
        }
    });
    
    $(".js-slideBottom").owlCarousel({
        loop: false,
        margin: 0,
        nav: true,
        dots: false,
        items: 1
    });
    
    $(".js-downArrow").on("click", function(){
        var scrollTo = $(".blockText").offset().top;
        $("html, body").animate({scrollTop: scrollTo}, 1000);
    });

});