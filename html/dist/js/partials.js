$(document).ready(function(){
    $('.js-openMenu').on("click", function () {
        $('.mobileMenu').addClass('opened');
        $('.js-overlay').fadeIn();
    });
    $('.js-close').on("click", function () {
        $('.mobileMenu').removeClass('opened');
        $('.js-overlay').fadeOut();
    });
    $('.js-overlay').on("click", function () {
        $('.js-close').trigger("click");
    });
});
